#-------------------------------------------------------------------------------------------------------------
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License. See https://go.microsoft.com/fwlink/?linkid=2090316 for license information.
#-------------------------------------------------------------------------------------------------------------

FROM python:3.8.1

ARG USER_ID=1101
ARG GROUP_ID=1101

# Avoid warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive \
    POETRY_VERSION=1.0.2 \
    PIP_VERSION=19.2.3

# Switch back to dialog for any ad-hoc use of apt-get
ENV DEBIAN_FRONTEND=

# Install openshift client
RUN wget https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz && \
    tar xvzf openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz && \
    cd openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit && \
    mv oc /usr/local/bin/ && \
    mv kubectl /usr/local/bin/ && \
    cd .. && \
    rm -rf openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit && \
    rm -rf openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit.tar.gz

# installer et configurer poetry - utilise $POETRY_VERSION à l'interne
RUN apt-get update \
  && apt-get install --no-install-recommends -y \
  # dépendance pour extraire le code coverage
  libxml2-utils

# Upgrade pip and globally install pipenv
RUN pip3 install --upgrade pip==$PIP_VERSION

# Install poetry
RUN pip3 install poetry==$POETRY_VERSION

# Permet l'utilisation de poetry pour les utilisateurs "with no name"
RUN mkdir /.config && chmod -R 777 /.config
    


