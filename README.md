# README #

### Description ###

Construire l'image docker nécessaire pour le CI/CD des projet python api. (pgw-gabarit-api)

### Comment utiliser l'image ###

Dans votre jenkins utilisé l'agent docker comme suit.
docker {
    image "docker-local.maven.ulaval.ca/pgw/pgw-python-build:latest"
    registryUrl 'https://docker-local.maven.ulaval.ca/v2'
    registryCredentialsId '...'
    args "-u root --entrypoint=''"                            
    reuseNode true
}

### Who do I talk to? ###

Martin Chénier
Daniel Bouchard